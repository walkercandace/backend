exports.isUserExists = async (DB, phoneNumber) => {
    const users = DB.collection('users').where('phone', '==', phoneNumber).get()
    const [userSnapshot] = await Promise.all([users])
    const usersArray = userSnapshot.docs;
    return usersArray.length > 0
}
