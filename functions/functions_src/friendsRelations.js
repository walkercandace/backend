const CONSTANTS = require('../helpers/constants')
const SMS = require('../helpers/sms')

exports.makeFriendsRelation = async (phoneNumbersList, context, DB, config, functions) => {
    let friend = null

    // Get MAX_CONTACTS_COUNT from config
    // if something wrong get local constant

    const configTemplate = await config.getTemplate();
    let MAX_CONTACTS_COUNT;
    try {
        const countValue = configTemplate.parameters.max_contacts_number.defaultValue.value;
        MAX_CONTACTS_COUNT = parseInt(countValue)
    } catch (e) {
        MAX_CONTACTS_COUNT = CONSTANTS.MAX_CONTACTS_COUNT_LOCAL
    }

    // One contact can have more then 1 phone

    if (!Array.isArray(phoneNumbersList)) {
        throw new functions.https.HttpsError('invalid-argument', 'Incoming data must be Array')
    }

    if (phoneNumbersList.length === 0) {
        throw new functions.https.HttpsError('invalid-argument', 'Incoming Array can not be empty')
    }

    // Get Current user
    const usersCollectionRef = DB.collection('users')
    const currentUserRef = usersCollectionRef.doc(context.auth.uid)

    // Current user contacts collection
    const currentUserContactCollectionRef = currentUserRef.collection('contacts')
    const currentUserContactsSnapshot = await currentUserContactCollectionRef.get()
    const contactsArray = currentUserContactsSnapshot.docs;

    // We can already have MAX friend count (client also check this - double check)
    if (
        contactsArray.length + phoneNumbersList.length > MAX_CONTACTS_COUNT
    ) {
        throw new functions.https.HttpsError('out-of-range', 'You can have only 5 friends')
    }

    const currentUserSnapshot = await currentUserRef.get()
    const currentUser = currentUserSnapshot.data()

    let friends = []
    let errors = []
    for(const phoneNumber of phoneNumbersList){
        // Find users that match incoming phones
        const usersWithIncomingPhonesRef = usersCollectionRef.where('phone', 'in', [phoneNumber]);

        const usersWithPhonesSnapshot = await usersWithIncomingPhonesRef.get()

        const usersArray = usersWithPhonesSnapshot.docs;

        // If more then 1 user was find throw Error
        // User Will select number on UI
        // If no users - send invite Maybe :)
        // now throw error
        if (usersArray.length > 1) {
            errors.push('Account ' + phoneNumber + ' was found')
            //throw new functions.https.HttpsError('out-of-range', 'Account was found', {accounts: usersArray})
        } else if (usersArray.length === 0) {
            const message = `${currentUser.phone} has invited you to install the Just Us app https://apps.apple.com/us/app/id1519468533`
            try{
                const sms = await SMS.sendSMS(phoneNumber, message)
                console.log('SMS RESULT', sms)
            }catch(error){
                console.log('SMS ERROR', error)
                errors.push("Can't send SMS to " + phoneNumber)
            }
            //throw new functions.https.HttpsError('not-found', 'Account not found')
            continue
        }

        // Get our friend data
        friend = usersArray[0].data()

        const exists = await currentUserContactCollectionRef.doc(friend.id).get()
        if (exists.data()) {
            //throw new functions.https.HttpsError('already-exists', 'You are already friends', friend)
            continue
        }

        // Friends

        // Friend saves to me in contacts
        await currentUserContactCollectionRef.doc(friend.id).set({
            ref: DB.doc('users/' + friend.id),
            status: 'new'
        })

        // I saves to friend in buddies
        await usersCollectionRef.doc(friend.id).collection('buddies').doc(currentUser.id).set({
            ref: DB.doc('users/' + currentUser.id)
        })

        friends.push()
    }

    if(errors.length){
        throw new functions.https.HttpsError('out-of-range', errors.join('. '))
    }

    return friends
}

exports.removeFriendshipRelation = () => {
}
