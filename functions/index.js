const SubCol = require('./helpers/userSubCollectionGetter')
const CONSTANTS = require('./helpers/constants')
const {getTokens} = require('./helpers/getTokensFromBounds')

const {isUserExists} = require('./functions_src/isUserExist')
const {makeFriendsRelation} = require('./functions_src/friendsRelations')

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const _ = require('lodash')
const NodeGeocoder = require('node-geocoder');

admin.initializeApp(functions.config().firebase);

const DB = admin.firestore();
const FCM = admin.messaging();
const config = admin.remoteConfig();
const storage = admin.storage();
const bucket = storage.bucket()

/****** isUserExist ************************************************************/
/**
 *
 * Checks if user exist in system; (For SignIn\SignUp process)
 * No Auth check
 * Returns Object {exists: true|false}
 *
 * */
exports.isUserExist = functions.https.onCall(async (phoneNumber, context) => {
    const exists = await isUserExists(DB, phoneNumber);
    return {exists}
});
/** ************************************************************************ **/

/****** makeFriendsRelation ***************************************************/
/**
 *
 * Create Friend Relation
 * Returns null - if user does not exist | user data if friend use app too
 *
 * */

exports.makeFriendsRelation = functions.https.onCall(async (phoneNumbersList, context) => {
    if (context.auth && context.auth.uid) {
        const friend = makeFriendsRelation(phoneNumbersList, context, DB, config, functions)
        return friend
    } else {
        throw new functions.https.HttpsError('unauthenticated', 'You must be authenticated')
    }
});
/** ************************************************************************ **/

/**
 *
 * Remove Friend Relation
 * Returns Success
 *
 * */
/****** removeFriendshipRelation **********************************************/
exports.removeFriendshipRelation = functions.https.onCall(async (id, context) => {
    if (context.auth && context.auth.uid) {
        await DB.collection('users').doc(context.auth.uid).collection('contacts').doc(id).delete()
        await DB.collection('users').doc(id).collection('buddies').doc(context.auth.uid).delete()
        return 'Success'
    } else {
        throw new functions.https.HttpsError('unauthenticated', 'You must be authenticated')
    }

})
/** ************************************************************************ **/

/****** removeBuddyRelation ************************************************************/
exports.removeBuddyRelation = functions.https.onCall(async (id, context) => {
    if (context.auth && context.auth.uid) {
        await DB.collection('users').doc(context.auth.uid).collection('buddies').doc(id).delete()
        await DB.collection('users').doc(id).collection('contacts').doc(context.auth.uid).delete()
        return {success: true}
    } else {
        throw new functions.https.HttpsError('unauthenticated', 'You must be authenticated')
    }
})
/** ************************************************************************ **/

/****** getContacts ************************************************************/
/**
 *
 * Return list of contacts
 *
 * */
exports.getContacts = functions.https.onCall(async (data, context) => {
    let contacts = []
    if (context.auth && context.auth.uid) {
        contacts = await SubCol.getSubCollection(context.auth.uid, 'contacts', DB)
    } else {
        throw new functions.https.HttpsError('unauthenticated', 'You must be authenticated')
    }
    return contacts
})
/** ************************************************************************ **/

/****** getBuddies ************************************************************/
/**
 *
 * Return list of buddies
 *
 * */
exports.getBuddies = functions.https.onCall(async (data, context) => {
    let buddies = []
    if (context.auth && context.auth.uid) {
        buddies = await SubCol.getSubCollection(context.auth.uid, 'buddies', DB)
    } else {
        throw new functions.https.HttpsError('unauthenticated', 'You must be authenticated')
    }
    return buddies
})
/** ************************************************************************ **/

/****** triggerEvent **********************************************************/
/**
 *
 * User can send e types of event
 * 'check_in', 'heads_up', 'help'
 *
 * */
exports.triggerEvent = functions.https.onCall(async (data, context) => {
    if (context.auth && context.auth.uid) {
        if (data.type && CONSTANTS.EVENTS_TYPES.includes(data.type)) {
            const currentUserContactCollection = await DB.collection('users').doc(context.auth.uid).collection('contacts').get()

            const timeStamp = Date.now();
            // Minimal event data
            // this event we will save to DB
            const event = {
                type: data.type,
                user_profile: DB.doc('users/' + context.auth.uid),
                server_timestamp: timeStamp
            }
            // Add optional data to event if data exist

            if (data.date) {
                event.date = data.date
            }
            if (data.location && data.location.latitude && data.location.longitude) {
                event.location = data.location

                const options = {
                    provider: 'google',
                    apiKey: 'AIzaSyAd8BJpptqA_DkjGvNP5Q2MJ9GF7Tsbbqs',
                    formatter: null // 'gpx', 'string', ...
                };

                const geocoder = NodeGeocoder(options);
                const address = await geocoder.reverse({ lat: data.location.latitude, lon: data.location.longitude });

                if(address && Array.isArray(address) && address.length > 0 && address[0]){
                    event.address = address[0].formattedAddress
                }
            }
            if (data.fb_stream_url) {
                event.streams = {
                    fb_stream_url: data.fb_stream_url
                }
            }
            if (data.mux_stream && data.mux_stream.id && data.mux_stream.playback) {
                event.streams = event.streams ? {
                    ...event.streams,
                    mux_stream_id: data.mux_stream.id,
                    mux_stream_playback: data.mux_stream.playback
                } : {
                    mux_stream_id: data.mux_stream.id,
                    mux_stream_playback: data.mux_stream.playback
                }
            }

            let recipients = []
            // If event type is 'help' and user has location service enable:
            // Get list of users with tokens that near ~ 1 mile
            if (data.type === 'help') {
                if (event.location) {
                    recipients = await getTokens(event.location, context, DB)
                }
            }

            // Add users from contacts
            for (const doc in currentUserContactCollection.docs) {
                let contact = currentUserContactCollection.docs[doc].data()

                const buddyDoc = await DB.collection('users').doc(contact.ref.id).collection('buddies').doc(context.auth.uid)
                let isBuddy = (await buddyDoc.get()).data()

                if (
                    contact.ref
                    && isBuddy
                ) {
                    recipients.push({
                        userId: contact.ref.id,
                        toks: (await contact.ref.collection('push_tokens').get()).docs
                    })
                }
            }

            recipients = _.uniqBy(recipients, 'userId')
            const info = await event.user_profile.get()
            const full_name = info.get('first_name') + ' ' + info.get('last_name')

            console.log('RECIPIENTS: ', recipients)
            for (const ind in recipients) {
                console.log('<>', recipients[ind].userId)
                if (recipients[ind].userId !== context.auth.uid) {
                    // Add event and get saved event data
                    const _ev = await DB.doc('users/' + recipients[ind].userId).collection('events').add(event)

                    // create event payload
                    let category = null;
                    if (event.location) {
                        category = "LOCATION"
                    }
                    if (event.streams) {
                        category = category ? `${category}+LIVE` : "LIVE"
                    }

                    const titles = {
                        'heads_up': 'Heads-Up Notification',
                        'check_in': 'Check-In Notification',
                        'help': 'Help Alert',
                    }

                    const body = {
                        'heads_up': `${full_name} may need help. Location is ${event.address}`,
                        'check_in': `${full_name}: I am safe.`,
                        'help': `${full_name} is in need of help. Location is ${event.address}`,
                    }

                    const payload = {
                        apns: {
                            payload: {
                                "aps": {
                                    "alert": {
                                        "title": titles[data.type],
                                        "body": body[data.type]
                                    },
                                    "sound": "default",
                                },
                                data: {
                                    "deeplink": "location",
                                    "event_id": _ev.id,
                                    "type": data.type
                                }
                            }
                        },
                    }
                    if (category) {
                        payload.apns.payload.aps.category = category
                    }

                    for (const tokenDoc in recipients[ind].toks) {
                        const token = recipients[ind].toks[tokenDoc].data()
                        if (token) {
                            payload.token = token.fcm_token ? token.fcm_token : null
                            if (payload.token) {
                                try {
                                    const messageResponse = await FCM.send(payload)
                                    console.log('success', messageResponse)
                                } catch (error) {
                                    console.log('error', error)
                                }
                            }
                        }
                    }
                }
            }
            return 'success'
        } else {
            throw new functions.https.HttpsError('invalid-argument', 'Incoming data must have required  field type  [\'check_in\', \'heads_up\', \'help\']')
        }
    } else {
        throw new functions.https.HttpsError('unauthenticated', 'You must be authenticated')
    }
})
/** ************************************************************************ **/

/****** getEvents *************************************************************/
exports.getEvents = functions.https.onCall(async (data, context) => {
    let events = []
    if (context.auth && context.auth.uid) {
        const eventsSnap = await DB.collection('users').doc(context.auth.uid).collection('events').get()
        for (const doc in eventsSnap.docs) {
            let docData = eventsSnap.docs[doc].data()
            // Some old test data has user object instead of reference
            try {
                if (docData.user_profile) {
                    docData.user_profile = (await docData.user_profile.get()).data()
                }
            } catch (e) {
                docData.user_profile = {
                    //
                }
            }

            docData.id = eventsSnap.docs[doc].id
            events.push(docData)
        }
    } else {
        throw new functions.https.HttpsError('unauthenticated', 'You must be authenticated')
    }
    return events
})
/** ************************************************************************ **/

/****** getEvent **************************************************************/
exports.getEvent = functions.https.onCall(async (eventId, context) => {
    let event = null
    if (context.auth && context.auth.uid) {
        const eventSnap = await DB.collection('users').doc(context.auth.uid).collection('events').doc(eventId).get()
        event = eventSnap.data()
        if (event && event.user_profile) {
            event.user_profile = (await event.user_profile.get()).data()
        }
        event.id = eventSnap.id
    } else {
        throw new functions.https.HttpsError('unauthenticated', 'You must be authenticated')
    }
    return event
})
/** ************************************************************************ **/

/****** removeOldEventsScheduleFunction ****************************************/
// Must be run every 2 days
// I try do that at 00:00 every 2 days

exports.removeOldEventsScheduleFunction = functions.pubsub.schedule('0 0 * * *').onRun(async (context) => {
     // Needs to delete old notifications

     const date = new Date();
     date.setDate(date.getDay() - 2)
     date.setHours(23);
     date.setMinutes(59);
     date.setSeconds(59);
     const timeStamp = date.setMilliseconds(999)
     const querySnapshot = await DB.collectionGroup('events').where('server_timestamp', '<', timeStamp).get();
     querySnapshot.forEach(function (doc) {
         doc.ref.delete()
     });
     return null;
})
/** ************************************************************************ **/

/****** deleteUser ************************************************************/
exports.deleteUser = functions.https.onCall(async (data, context) => {
    // Remove steps
    // 1. Remove current user from all contacts collection from all other users
    // 2. Remove current user from all buddies collection from all other users
    // 3. Remove current user from all events collection from all other users
    // 4. Remove current user avatar
    // 5. Remove current user from users collection
    if (context.auth && context.auth.uid) {
        const currentUserReference = DB.collection('users').doc(context.auth.uid)
        if (currentUserReference) {
            // Steps 1-3
            const [contacts, buddies, events] = await Promise.all([
                DB.collectionGroup('contacts').where('ref', '==', currentUserReference).get(),
                DB.collectionGroup('buddies').where('ref', '==', currentUserReference).get(),
                DB.collectionGroup('events').where('user_profile', '==', currentUserReference).get()
            ]);
            const allDocuments = [...contacts.docs, ...buddies.docs, ...events.docs]
            allDocuments.forEach(doc => {
                doc.ref.delete()
            })
            // Step 4
            const fileRef = bucket.file(`avatars/${context.auth.uid}.jpg`);
            if ((await fileRef.exists())[0]) await fileRef.delete()
            // Step 5
            await currentUserReference.delete()
        }
        return {success: true}
    } else {
        throw new functions.https.HttpsError('unauthenticated', 'You must be authenticated')
    }
})

exports.setLocationTimestampOnCreate = functions.firestore.document('users/{userId}/locations/{locationId}').onCreate((change, context) => {
    return change.ref.set({
        timestamp: parseInt(Date.now()/1000)
    }, { merge: true });
});

exports.setLocationTimestampOnUpdate = functions.firestore.document('users/{userId}/locations/{locationId}').onUpdate((event, context) => {
    const before = event.before.data()
    const after = event.after.data()

    if(before.latitude === after.latitude && before.longitude === after.longitude){
        return
    }

    return event.after.ref.set({timestamp: parseInt(Date.now()/1000)}, {merge: true});
});


/** ************************************************************************ **/
