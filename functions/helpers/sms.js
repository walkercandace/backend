//https://www.twilio.com/docs/sms/quickstart/node

const accountSid = 'AC2f6ceca228c0971db76368efba873c45';//LIVE: 'AC2f6ceca228c0971db76368efba873c45';TEST 'AC41e5fcd3472acd1e7aea4ffe350c2b4a';
const authToken = '0eb85216a620c68e690cd943c9014873';//LIVE: '0eb85216a620c68e690cd943c9014873';TEST '0eb85216a620c68e690cd943c9014873'
const fromNumber = '+14805685235';
const client = require('twilio')(accountSid, authToken);

exports.sendSMS = (to, sms) =>
    client.messages
      .create({
         body: sms,
         from: fromNumber,
         to: to
       })
