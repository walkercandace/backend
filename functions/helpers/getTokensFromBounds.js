exports.getTokens = async (location, context, DB) => {

    const SEARCH_RADIUS = 3.2; // Kms ~ 1mile
    const {latitude, longitude} = location;

    const lat_change = SEARCH_RADIUS / 111.2;
    const lon_change = Math.abs(Math.cos(latitude * (Math.PI / 180)));

    const bounds = {
        lat_min: latitude - lat_change,
        lon_min: longitude - lon_change,
        lat_max: latitude + lat_change,
        lon_max: longitude + lon_change
    };

    const lastHour = parseInt(Date.now()/1000) - 3600

    // Firebase does not allow to combine this queries

    let locationsLat = DB.collectionGroup('locations')
        .where('latitude', '>', bounds.lat_min)
        .where('latitude', '<', bounds.lat_max);

    let locationsLon = DB.collectionGroup('locations')
        .where('longitude', '>', bounds.lon_min)
        .where('longitude', '<', bounds.lon_max);

    /*
       unfortunately, Firebase doesn't support compound indexes with a non unique fields
       that is why, we have to filter locations by a timestamp manually
     */
    let timestampFilter = function(doc) {
        return doc.timestamp > lastHour;
    };
    const locsLatlist = (await locationsLat.get()).docs.filter(timestampFilter);
    const locsLonlist = (await locationsLon.get()).docs.filter(timestampFilter);

    const filter = []
    let tokens = []
    const returnData = []

    for (const doc in locsLatlist) {
        const user = locsLatlist[doc].ref.parent.parent.id
        // from 1 query we skip only current user
        if (context.auth.uid !== user) filter.push(user)
    }

    for (const doc in locsLonlist) {
        // from second we need only that already exist in filter
        if (filter.includes(locsLonlist[doc].ref.parent.parent.id)) {
            tokens = [...tokens, {
                userId: locsLonlist[doc].ref.parent.parent.id,
                toks: (await locsLonlist[doc].ref.parent.parent.collection('push_tokens').get()).docs
            }]
        }
    }

    for (const doc in tokens) {
        // Skip empty array with tokens
        if (tokens[doc].toks.length) {
            returnData.push(tokens[doc])
        }
    }
    return returnData
}
