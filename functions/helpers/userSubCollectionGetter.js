exports.getSubCollection = async (userId, collectionName, DB) => {
    const docs = []
    const userCollection = await DB.collection('users').doc(userId).collection(collectionName).get()
    for (const doc in userCollection.docs) {
        let docData = userCollection.docs[doc].data()

        //ref - its reference to document
        // we need to get data from reference

        if (docData.ref) {
            const refInfo = await docData.ref.get()
            docData = {
                ...docData,
                user_profile: refInfo.data()
            }
            delete docData.ref
            docs.push(docData)
        }
    }
    return docs
}
