# Just Us Backend services

---

##NOTE: 
    current removeOldEventsSchedulFunction - is not working in free plan
    error ocurs while deploy it: 
    `HTTP Error: 400, Billing must be enabled for activation of service '[cloudscheduler.googleapis.com]' in project '284895225587' to proceed`
    So needs to be tested after paid plan will be enabled
    

___

## Environment
1. Node v12.16.1
2. `npm install -g firebase-tools`

---

## Start work with project
1. Pull down code from git
2. Enter functions folder inside project
3. Run npm install
4. Move back to project root directory
5. Login to firebase cli (`npm install -g firebase-tools` must be installed) 
    1. `firebase login` Will open google auth login window
    2. Select account that has access to firebase project
    3. Select project `just-us` or `just-us-stage`.
	4. Switch between *prod* and *stage* firebase projects
		`firebase use just-us` - prod
		`firebase use just-us-stage` - stage
6. Write Code :)
7. `firebase deploy --only functions` - deploy all functions  
   `firebase deploy --only functions:function_name` - deploy function with name `function_name`
   `firebase deploy` - deploy all project data

---

## About Project Data
1. `functions` folder - all staff for functions
2. `firestore.indexes.json`, `firestore.rules` - indexes and rules for firestore

##### NOTE:
    All data already in firebase project. And we just pull down code from git and connect firebase cli to remote project.
    
---

## When we finally will have access to client created firebase project.
   - Change login project in firebase cli
   - Change projectid in `.firebaserc` file at the root of project
   - `firebase deploy` - to push code to new project
---   

